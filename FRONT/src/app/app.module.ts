import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { PalindrometestComponent } from './palindrometest/palindrometest.component';
import { PalindromeService } from './services/palindrome.service';

@NgModule({
  declarations: [
    AppComponent,
    PalindrometestComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [PalindromeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
