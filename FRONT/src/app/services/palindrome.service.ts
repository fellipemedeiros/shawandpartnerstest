import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PalindromeService {

  constructor(private http: HttpClient) { }

  checkPalindrome(params) {
    return this.http.get("http://localhost:8081/palindrome/" + params, {responseType: 'text'});
  }
}
