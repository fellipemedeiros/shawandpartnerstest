import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PalindromeService } from '../services/palindrome.service';


@Component({
  selector: 'app-palindrometest',
  templateUrl: './palindrometest.component.html',
  styleUrls: ['./palindrometest.component.css']
})
export class PalindrometestComponent implements OnInit {

  result : string = "";

  constructor(private palindromeService: PalindromeService) { }

  ngOnInit() {
  }

  checkPalindrome = (words) => {
    this.palindromeService.checkPalindrome(words).subscribe((data: any) => {
      this.result = data;
    }, err => {
      this.result = err.error;
    });
  };

}
