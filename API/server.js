'use strict';

const Hapi=require('hapi'); 

const server=Hapi.server({
    host:'localhost',
    port:8081
});

const handler = function (request, h) {
    if(checkPalindrome(request.params.word)) {
        return h.response("Is a palindrome").code(200);
    } else {
        return h.response("It's not a palindrome").code(400);
    }
};

server.route({ 
    method: 'GET', 
    path: '/palindrome/{word}', 
    handler,
    config: {
        cors: {
            origin: ['*'],
            additionalHeaders: ['cache-control', 'x-requested-with']
        }
    }
 });

async function start() {
    
    try {
        await server.start();
    }
    catch (err) {
        console.log(err);
        process.exit(1);
    }

    console.log('Server running at:', server.info.uri);
};

start();

function checkPalindrome(wordToCheck) {
    const regex = /[\W_]/g;
    var wordToCheckLower = wordToCheck.toLowerCase().replace(regex, '');
    var reversewordToCheck = wordToCheckLower.split('').reverse().join(''); 
    return reversewordToCheck === wordToCheckLower;
}